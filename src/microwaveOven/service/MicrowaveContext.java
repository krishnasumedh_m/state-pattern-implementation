package microwaveOven.service;

import microwaveOven.util.Results;

public class MicrowaveContext 
{
	 final  int setClock   = 1000;
	 final  int waitIp     = 1001;
	 final  int setStart   = 1002;
	 final  int cancelStop = 1003;
	 	 
	 MicrowaveStateI idle ;
	 MicrowaveStateI waitIP ;
	 MicrowaveStateI cooking ;
	 MicrowaveStateI cookingPaused ;
	 MicrowaveStateI cookingCompleted ;
	 
	 MicrowaveStateI currentState ;
	 microwaveOven.util.Results results;
	
    public MicrowaveContext()
    {
    	results = new microwaveOven.util.Results();
    	idle = new Idle(this);
    	waitIP = new WaitIP(this);
    	cooking = new Cooking(this);
    	cookingPaused = new CookingPaused(this);
    	
    	currentState = idle; 	
    }
    
    
    public Results getResults()
    {	
    	return results;
    }
    
    public void setClock()
    {
    	currentState.action(setClock);
    }
    
    public void timeInput(int val)
    {
    	currentState.action(val);
    	
    }
    
    
    
    public void setStart()
    {
    	currentState.action(setStart);
    	
    }
    
    public void cancelStop()
    {
    	currentState.action(cancelStop);
    	
    }
    
    void setState(MicrowaveStateI state) 
    {
		this.currentState = state;
	}
    
    public MicrowaveStateI getCurrentState() 
    {
        return currentState;
    }
    

    public MicrowaveStateI getIdleState() 
    {
        return idle;
    }

    public MicrowaveStateI getWaitIPState() 
    {
        return waitIP;
    }

    public MicrowaveStateI getCookingState() 
    {
        return cooking;
    }

    public MicrowaveStateI getCookingPausedState() 
    {
        return cookingPaused;
    }
    
    public MicrowaveStateI getCookingCompletedState() 
    {
        return cookingCompleted;
    }
    
}
