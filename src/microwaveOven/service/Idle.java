package microwaveOven.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Idle implements MicrowaveStateI 
{
	MicrowaveContext mc;

	public Idle(MicrowaveContext mc)
	{
		  this.mc = mc;
		  mc.results.storeNewResult("--------------------------------");
		  
		  DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");
		  LocalDateTime now = LocalDateTime.now();
		  mc.results.storeNewResult("Time : " + dtf.format(now));
	}
		
	
	@Override
	public void action(int keycode) 
	{
		
		switch(keycode)
		{
		  case 1000:  
			         mc.results.storeNewResult("Microwave : IDLE -> Waiting for Time input");
			         mc.setState(mc.getWaitIPState());
			         break;		         
			       
		  case 1001:
			        //System.out.println("");
		            //mc.setState(mc.getWaitIPState());
		            break;
		        
		  case 1002:
			  		 mc.results.storeNewResult("You need to press SetClock to set the time before  pressing Set/Start.");
			        break;  
			        
		  case 1003:
			  		 mc.results.storeNewResult("Invalid Operation : Cancel/Stop invalid unless cooking starts. !! ");
		            break;     
		            		            
		  default :
			  		mc.results.storeNewResult("Select an action before pressing the Digits !! ");
			        break;	
		
		}
		
	} 
 

	
}
