package microwaveOven.service;

public class Cooking implements MicrowaveStateI 
{
	MicrowaveContext mc;
	static int time;
	
	public Cooking(MicrowaveContext mc)
	{
		this.mc = mc;
		
	}

	@Override
	public void action(int keycode) 
	{
		switch(keycode)
		{
		  case 1000:  
			  		 mc.results.storeNewResult("Cannot SetClock.. Cooking in progress..."); 
			         break;		         
			       
		  case 1001:
			        //System.out.println("");
		            //mc.setState(mc.getWaitIPState());
		            break;
		        
		  case 1002:
			        cooking();
			        break;  
			        
		  case 1003:
			        // Pause the oven
			  		mc.results.storeNewResult("Microwave Cooking --> Microwave Paused ...");
			        mc.setState( mc.getCookingPausedState() );
			       // System.out.println("Current State: " + mc.currentState.getClass().getSimpleName());
		            break;     
		            		            
		  default :
			        //upon pressing the numbers.
			  		mc.results.storeNewResult("Cooking going on. Don't press any random digits !! .");
			        break;	
		
		}
				
	}
	
	public static void setTime(int t)
	{
		time =  t;	
	}

	
	public void cooking()   
	{	
		mc.results.storeNewResult("Cooking in Progress.");
  //	System.out.println("Cook time: "+time +" seconds"); //time 
		
		/*	while(time != 0)
		{
			time -- ;
			
		}
			
		if(time == 0)
		{
		System.out.println("time : "+time +" seconds");	
		System.out.println("Cooking completed");
	   //mc.setState( mc.getCookingCompletedState() );
		}	
		*/
			
	}	

}
