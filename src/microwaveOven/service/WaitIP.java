package microwaveOven.service;

public class WaitIP implements MicrowaveStateI 
{
    //Cooking c = new Cooking();
	MicrowaveContext mc;
	static int time;
	int flag = 0;

	public WaitIP(MicrowaveContext mc)
	{
		this.mc = mc;
	}

	@Override
	public void action(int keycode) 
	{
		switch(keycode)
		{
		
		 case 1000:  
			         if(flag==0)
			             mc.results.storeNewResult("You have already pressed the SetClock. Now enter the cooking time."); 	 
			         else
			             mc.results.storeNewResult("After entering the cooking time, you need to press SetStart."); 	 
			     
			         //mc.setState(mc.getWaitIPState());
			         break;		         
			       
		 case 1001:
			        //System.out.println("");
		            //mc.setState(mc.getWaitIPState());
		            break;
		        
		 case 1002:
			        if(flag == 0)
			        { 
			         mc.results.storeNewResult("Enter the cooking time before pressing SetStart.");	
			        }
			        else
			        {	
			 		mc.setState( mc.getCookingState() );
			 		mc.results.storeNewResult("Cooking Started.. ");
			 		mc.setStart();
			        }
			        break;  
			        
		 case 1003:
			 		mc.results.storeNewResult("Cooking Cancelled !!.");
			 		mc.results.storeNewResult("Back to idle state..");
			 		mc.setState( mc.getIdleState() );
			 	//	System.out.println("Current State: " + mc.currentState.getClass().getSimpleName());
		            break;     
		            		            
		 default :
			        time = keycode;
			        flag = 1;
			        mc.results.storeNewResult("Time entered :" + keycode+ " seconds.");
			        Cooking.setTime(time);
			        break;	
		
		}
		
	}
	
	
	
	
	
}
