package microwaveOven.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CookingPaused implements MicrowaveStateI 
{

	MicrowaveContext mc;
	public CookingPaused(MicrowaveContext mc)
	{
		this.mc = mc;
	}
	
	
	
	@Override
	public void action(int keycode) 
	{
		switch(keycode)
		{
		  case 1000:  
			  		  mc.results.storeNewResult("SetClock Inactive during cookingPaused...");
			          break;		         
			       
		  case 1001:
			        
		            break;
		        
		  case 1002:
			  		mc.results.storeNewResult("Cooking Paused --> Cooking Continuing...");
			  		mc.setState( mc.getCookingState() );
	//  		    System.out.println("Current State: " + mc.currentState.getClass().getSimpleName());
			        break;  
			        
		  case 1003:
			  		mc.results.storeNewResult("Cooking Paused --> Cooking Stopped !!.");
			  		mc.results.storeNewResult("Back to idle state..");	
			        mc.setState( mc.getIdleState() );
	//		        System.out.println("Current State: " + mc.currentState.getClass().getSimpleName());
			        Cooking.time = 0;  // Time Reset.
			        
			         
			        mc.results.storeNewResult("-------------RESET---------------");
			       
			        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");
					LocalDateTime now = LocalDateTime.now();
					mc.results.storeNewResult( "Time : " + dtf.format(now) );
		            break;     
		            		            
		  default :
			  		mc.results.storeNewResult("Digit Buttons Inactive during cookingPaused..");
			        break;	
		
		}


	}

}
