package microwaveOven.driver;
import microwaveOven.util.FileProcessor;
import microwaveOven.util.Results;
import microwaveOven.util.FileDisplayInterface;
import microwaveOven.service.MicrowaveContext;


public class Driver 
{
	String line = null;
	static String ofname=null;
	
	public void readL(Driver d,FileProcessor fp,MicrowaveContext mc ) 
	{
		
		while( null != ( line = fp.readLine() ) )
	     {		 
			if(line.equals("SetClock") )
			{
				mc.setClock();
			}
			else if( line.equals("SetStart") )  
			{
				mc.setStart();
			}
			else if(line.equals("CancelStop") )   
			{	
				mc.cancelStop(); 	
			}
			else
			{
				 try
				    {
				    int value = Integer.parseInt(d.line); // time in seconds
				    //System.out.println(value);
				    mc.timeInput(value);
				   
				    }
			        
				    catch (NumberFormatException e)
				    {
				    System.err.println("NumberFormatException");
				    e.printStackTrace();
				    System.exit(0);
				    }
				
			}
	        
	     } 
		
	}

	public static void main(String[] args) 
	{
		
		 if( (args.length != 2))
		   {
			   System.err.println("Invalid Arguments! Format : <input.txt> <output.txt> \n");
			   System.exit(0);
		   }
		 
		 FileProcessor fp = new FileProcessor(args[0]);
         ofname = args[1];
		
		Driver d = new Driver();
		Results results ;
		MicrowaveContext mc = new MicrowaveContext();
     
		               //ofname = "out.txt";
		 			  //FileProcessor fp = new FileProcessor("input.txt");
		        
			      d.readL(d,fp,mc);
			      
			      results = mc.getResults();
			      
			      String res = results.getResult();
			      
			      results.writeToFile(ofname);

	    			      			      
	 }


}
