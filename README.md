Implemented the state pattern in java based on the functionality of a Microwave oven and 
demonstrated all possible transitions between different states present in a microwave oven 
(such as cooking,cooking paused,idle etc.) upon receiving inputs from the user.

Ref - https://en.wikipedia.org/wiki/State_pattern
